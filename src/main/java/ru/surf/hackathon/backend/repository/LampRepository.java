package ru.surf.hackathon.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.surf.hackathon.backend.entity.Lamp;

public interface LampRepository extends JpaRepository<Lamp, String> {
}
