package ru.surf.hackathon.backend.scheduler;

import com.opencsv.bean.CsvToBeanBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.surf.hackathon.backend.dto.LampBean;
import ru.surf.hackathon.backend.service.LampFeignClient;
import ru.surf.hackathon.backend.service.LampService;

import java.io.StringReader;
import java.util.List;

@Component
@RequiredArgsConstructor
public class LampScheduler {

    private final LampService service;

    private final LampFeignClient lampClient;

    @Scheduled(cron = "@midnight")
    public void refreshLamps() {
        String imagesString = lampClient.getLampsAsString();

        if (imagesString == null) {
            return;
        }

        List<LampBean> lampBeans = new CsvToBeanBuilder<LampBean>(new StringReader(imagesString))
                .withType(LampBean.class)
                .withSeparator(';')
                .build()
                .parse();

        service.saveLamps(lampBeans);
    }
}
