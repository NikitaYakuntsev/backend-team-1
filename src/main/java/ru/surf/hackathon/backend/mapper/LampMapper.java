package ru.surf.hackathon.backend.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.surf.hackathon.backend.dto.LampBean;
import ru.surf.hackathon.backend.dto.LampDTO;
import ru.surf.hackathon.backend.entity.Currency;
import ru.surf.hackathon.backend.entity.Lamp;
import ru.surf.hackathon.backend.entity.Switch;
import ru.surf.hackathon.backend.service.LampFeignClient;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Component
@RequiredArgsConstructor
public class LampMapper {

    private final LampFeignClient lampClient;


    public Lamp toEntity(LampBean lampBean) {
        if (lampBean == null) {
            return null;
        }

        Lamp lamp = new Lamp();

        String barcode = lampBean.getBarcode();
        if (barcode == null || barcode.length() != 13) {
            return null;
        } else {
            lamp.setBarcode(barcode);
        }

        lamp.setImage(lampBean.getImage());
        lamp.setBrand(lampBean.getBrand());
        lamp.setModel(lampBean.getModel());
        lamp.setDescription(lampBean.getDescription());

        if (lampBean.getRub() != null) {
            lamp.setPrice(lampBean.getRub());
            lamp.setCurrency(Currency.RUB);
        } else if (lampBean.getUsd() != null) {
            lamp.setPrice(lampBean.getUsd());
            lamp.setCurrency(Currency.USD);
        } else {
            lamp.setPrice(BigDecimal.ZERO);
            lamp.setCurrency(Currency.UNKNOWN);
        }

        BigDecimal power = lampBean.getPower();
        BigDecimal lumen = lampBean.getLumen();

        lamp.setPower(power);
        lamp.setLumen(lumen);
        if (power != null && lumen != null) {
            lamp.setEfficiency(power.divide(lumen, 2, RoundingMode.HALF_UP));
        }

        lamp.setPowerEquivalent(lampBean.getPowerEquivalent());
        lamp.setColor(lampBean.getColor());
        lamp.setCri(lampBean.getCri());
        lamp.setAngle(lampBean.getAngle());
        lamp.setFlicker(lampBean.getFlicker());
        lamp.setRating(lampBean.getRating());

        Integer switchType = lampBean.getSwitchType();
        if (switchType == null || switchType == 0) {
            lamp.setSwitchType(Switch.UNKNOWN);
        } else {
            lamp.setSwitchType(Switch.fromInt(switchType));
        }

        String warranty = lampBean.getWarranty();
        if (warranty == null || warranty.isBlank() || warranty.charAt(0) == '-') {
            lamp.setWarranty(0);
        } else {
            lamp.setWarranty(Integer.parseInt(warranty));
        }

        Integer activity = lampBean.getActivity();
        lamp.setActive(activity != null && activity != 0);

        return lamp;
    }

    public LampDTO toEntity(Lamp lamp) {
        if (lamp == null) {
            return null;
        }

        String image = lamp.getImage();

        return new LampDTO(
                lamp.getBarcode(),
                image == null || image.isBlank() ? null : lampClient.getBase64Image(image),
                lamp.getBrand(),
                lamp.getModel(),
                lamp.getDescription(),
                lamp.getPrice(),
                lamp.getCurrency(),
                lamp.getPower(),
                lamp.getLumen(),
                lamp.getEfficiency(),
                lamp.getPowerEquivalent(),
                lamp.getColor(),
                lamp.getCri(),
                lamp.getAngle(),
                lamp.getFlicker(),
                lamp.getSwitchType(),
                lamp.getRating(),
                lamp.getWarranty(),
                lamp.getActive());
    }
}
