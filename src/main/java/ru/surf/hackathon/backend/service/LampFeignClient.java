package ru.surf.hackathon.backend.service;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

@Validated
public interface LampFeignClient {

    String getLampsAsString();

    String getBase64Image(@NotNull @NotBlank String imageName);
}
