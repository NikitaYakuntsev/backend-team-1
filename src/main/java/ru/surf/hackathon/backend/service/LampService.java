package ru.surf.hackathon.backend.service;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import org.springframework.validation.annotation.Validated;
import ru.surf.hackathon.backend.dto.LampBean;
import ru.surf.hackathon.backend.dto.LampDTO;
import ru.surf.hackathon.backend.validation.validator.ValidBarcode;

import java.util.List;

@Validated
public interface LampService {

    LampDTO getLampByBarcode(@NotNull @NotBlank @ValidBarcode String barcode);

    void saveLamps(@NotNull @Size(min = 2000) List<LampBean> lampBeans);
}
