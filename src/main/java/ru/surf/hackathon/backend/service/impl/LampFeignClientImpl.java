package ru.surf.hackathon.backend.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import ru.surf.hackathon.backend.service.LampFeignClient;

import java.util.Base64;

@Service
@RequiredArgsConstructor
public class LampFeignClientImpl implements LampFeignClient {

    private final String BASED_URL = "https://lamptest.ru";

    private final WebClient.Builder webBuilder;

    public String getLampsAsString() {
        return webBuilder.baseUrl(BASED_URL).build()
                .get()
                .uri(builder -> builder
                        .path("/led.csv")
                        .build())
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }

    @Override
    public String getBase64Image(String imageName) {
        String image = webBuilder.baseUrl(BASED_URL).build()
                .get()
                .uri(builder -> builder
                        .path("/images/photo/%s.jpg".formatted(imageName))
                        .build())
                .retrieve()
                .bodyToMono(String.class)
                .block();

        if (image == null) {
            return null;
        }

        return Base64.getEncoder().encodeToString(image.getBytes());
    }
}
