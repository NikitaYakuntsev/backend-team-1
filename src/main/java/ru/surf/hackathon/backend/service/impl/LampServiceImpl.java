package ru.surf.hackathon.backend.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.surf.hackathon.backend.dto.LampBean;
import ru.surf.hackathon.backend.dto.LampDTO;
import ru.surf.hackathon.backend.entity.Lamp;
import ru.surf.hackathon.backend.exception.notfound.NotFoundException;
import ru.surf.hackathon.backend.mapper.LampMapper;
import ru.surf.hackathon.backend.repository.LampRepository;
import ru.surf.hackathon.backend.service.LampService;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LampServiceImpl implements LampService {

    private final LampMapper mapper;

    private final LampRepository repository;

    public LampDTO getLampByBarcode(String barcodeString) {
        Lamp lamp = repository.findById(barcodeString)
                .orElseThrow(() -> new NotFoundException("Barcode not found"));

        return mapper.toEntity(lamp);
    }

    public void saveLamps(List<LampBean> lampBeans) {
        Set<Lamp> lampsToSave = lampBeans.stream()
                .map(mapper::toEntity)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());

        Set<Lamp> savedLamps = new HashSet<>(repository.findAll());

        lampsToSave.removeAll(savedLamps);

        repository.saveAll(lampsToSave);
    }
}
