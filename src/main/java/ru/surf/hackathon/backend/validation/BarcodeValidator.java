package ru.surf.hackathon.backend.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import lombok.RequiredArgsConstructor;
import ru.surf.hackathon.backend.validation.validator.ValidBarcode;

import java.util.regex.Pattern;

@RequiredArgsConstructor
public class BarcodeValidator implements ConstraintValidator<ValidBarcode, String> {

    @Override
    public boolean isValid(String barcode, ConstraintValidatorContext context) {
        if (barcode == null) {
            return true;
        }

        return barcode.length() == 13 && Pattern.compile("[0-9]+").matcher(barcode).matches();
    }
}