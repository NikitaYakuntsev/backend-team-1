package ru.surf.hackathon.backend.validation.validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import ru.surf.hackathon.backend.validation.BarcodeValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = BarcodeValidator.class)
public @interface ValidBarcode {

    String message() default "is not valid";

    Class<? extends Payload>[] payload() default {};

    Class<?>[] groups() default {};
}
