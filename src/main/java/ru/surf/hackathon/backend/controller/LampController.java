package ru.surf.hackathon.backend.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.surf.hackathon.backend.dto.LampDTO;
import ru.surf.hackathon.backend.scheduler.LampScheduler;
import ru.surf.hackathon.backend.service.LampService;

@RestController
@RequestMapping("/api/lamps")
@RequiredArgsConstructor
public class LampController {

    private final LampService lampService;

    private final LampScheduler scheduler;

    @GetMapping("/{barcode}")
    public LampDTO getLampByBarcode(@PathVariable String barcode) {
        return lampService.getLampByBarcode(barcode);
    }

    @GetMapping("/refresh")
    public void refreshLamps() {
        scheduler.refreshLamps();
    }
}
