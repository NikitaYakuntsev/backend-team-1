package ru.surf.hackathon.backend.entity;

public enum Currency {
    UNKNOWN,
    RUB,
    USD
}
