package ru.surf.hackathon.backend.entity;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Switch {
    UNKNOWN(0),
    GLOWS(1),
    GLOWS_WEAKLY(2),
    NOT_GLOWS(3);

    public final int value;

    Switch(int value) {
        this.value = value;
    }

    public static Switch fromInt(int value) {
        Switch[] values = values();

        if (value < 0 || value >= values.length) {
            return null;
        }

        return values[value];
    }

    @JsonValue
    public String getValue() {
        return this.toString();
    }
}
