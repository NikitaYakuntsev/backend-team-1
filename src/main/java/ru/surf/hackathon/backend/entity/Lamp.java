package ru.surf.hackathon.backend.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;

import java.math.BigDecimal;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "lamps")
@NoArgsConstructor
@AllArgsConstructor
public class Lamp {
    @Id
    private String barcode;
    private String image;
    private String brand;
    private String model;
    private String description;
    private BigDecimal price;
    @Enumerated(EnumType.STRING)
    private Currency currency;
    private BigDecimal power;
    private BigDecimal lumen;
    private BigDecimal efficiency;
    @Column(name = "power_equivalent")
    private BigDecimal powerEquivalent;
    private BigDecimal color;
    private BigDecimal cri;
    private BigDecimal angle;
    private BigDecimal flicker;
    @Column(name = "switch_type")
    @Enumerated(EnumType.ORDINAL)
    private Switch switchType;
    private BigDecimal rating;
    private int warranty;
    private Boolean active;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Lamp lamp = (Lamp) o;
        return barcode != null && Objects.equals(barcode, lamp.barcode);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
