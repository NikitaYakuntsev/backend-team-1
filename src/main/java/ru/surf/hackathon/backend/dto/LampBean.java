package ru.surf.hackathon.backend.dto;

import com.opencsv.bean.CsvBindByName;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class LampBean {

    @CsvBindByName
    private String barcode;

    @CsvBindByName(column = "lamp_image")
    private String image;

    @CsvBindByName
    private String brand;

    @CsvBindByName
    private String model;

    @CsvBindByName(column = "lamp_desc")
    private String description;

    @CsvBindByName
    private BigDecimal rub;

    @CsvBindByName
    private BigDecimal usd;

    @CsvBindByName(column = "p")
    private BigDecimal power;

    @CsvBindByName(column = "lm")
    private BigDecimal lumen;

    @CsvBindByName(column = "eq")
    private BigDecimal powerEquivalent;

    @CsvBindByName
    private BigDecimal color;

    @CsvBindByName
    private BigDecimal cri;

    @CsvBindByName
    private BigDecimal angle;

    @CsvBindByName
    private BigDecimal flicker;

    @CsvBindByName(column = "switch")
    private Integer switchType;

    @CsvBindByName
    private BigDecimal rating;

    @CsvBindByName(column = "war")
    private String warranty;

    @CsvBindByName(column = "act")
    private Integer activity;

}