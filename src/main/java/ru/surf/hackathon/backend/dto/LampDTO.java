package ru.surf.hackathon.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.surf.hackathon.backend.entity.Currency;
import ru.surf.hackathon.backend.entity.Switch;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class LampDTO {
    private String barcode;
    private String image;
    private String brand;
    private String model;
    private String description;
    private BigDecimal price;
    private Currency currency;
    private BigDecimal power;
    private BigDecimal lumen;
    private BigDecimal efficiency;
    private BigDecimal powerEquivalent;
    private BigDecimal color;
    private BigDecimal cri;
    private BigDecimal angle;
    private BigDecimal flicker;
    private Switch switchType;
    private BigDecimal rating;
    private int warranty;
    private boolean active;
}
