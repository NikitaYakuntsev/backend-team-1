package ru.surf.hackathon.backend.exception.handler;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.surf.hackathon.backend.exception.model.ErrorDto;

import java.util.List;

@ControllerAdvice
public class ConstraintViolationExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorDto> handleConstraintViolationException(ConstraintViolationException e) {
        List<String> errors = e.getConstraintViolations().stream()
                .map(this::createErrorMessage)
                .filter(StringUtils::isNotBlank)
                .toList();

        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new ErrorDto(errors));
    }

    private <T> String createErrorMessage(ConstraintViolation<T> cv) {
        String propertyName = getPropertyName(cv.getPropertyPath().toString());
        return propertyName + " " + cv.getMessage();
    }

    private String getPropertyName(String propertyPath) {
        String[] split = StringUtils.split(propertyPath, ".");
        if (split.length == 0) {
            return "";
        }
        return split[split.length - 1];
    }
}
