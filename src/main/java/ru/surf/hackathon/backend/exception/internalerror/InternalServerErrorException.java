package ru.surf.hackathon.backend.exception.internalerror;

public class InternalServerErrorException extends RuntimeException {

    public InternalServerErrorException(String message) {
        super(message);
    }
}
