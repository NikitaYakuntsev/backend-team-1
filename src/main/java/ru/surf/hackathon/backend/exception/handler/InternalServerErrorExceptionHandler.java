package ru.surf.hackathon.backend.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.surf.hackathon.backend.exception.internalerror.InternalServerErrorException;
import ru.surf.hackathon.backend.exception.model.ErrorDto;

import java.util.List;

@ControllerAdvice
public class InternalServerErrorExceptionHandler {

    @ExceptionHandler(InternalServerErrorException.class)
    public ResponseEntity<ErrorDto> handleInternalServerErrorException(InternalServerErrorException exception) {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ErrorDto(List.of(exception.getMessage())));
    }
}
