create table lamps
(
    barcode varchar primary key,
    image varchar,
    brand varchar,
    model varchar,
    description varchar,
    price numeric,
    currency varchar,
    power numeric,
    lumen numeric,
    efficiency numeric,
    power_equivalent numeric,
    color numeric,
    cri numeric,
    angle numeric,
    flicker numeric,
    switch_type int,
    rating numeric,
    warranty int,
    active bool
)